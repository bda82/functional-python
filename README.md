# Функциональный питон (рабочее название)

## Содержание

1. [О чем эта книга](./1_О_чем_эта_книга/README.md)
2. [Машина Тьюринга и проблема классического подхода к вычислениям](./2_Машина_Тьюринга_и_проблема_классического_подхода_к_вычислениям/README.md)
3. [Функциональное мышление](./3_Функциональное_мышление/README.md)
4. [Что не так с ООП, мутабельность и множественность состояний](./4_Почему_люди_не_понимают_функциональное_программирование/README.md)
5. [Почему люди не понимают функциональное программирование](./5_Почему_люди_не_понимают_функциональное_программирование/README.md)
6. [Почему многие люди потом выбирают функциональный подход](./6_Почему_многие_потом_выбирают_функциональное_программирование/README.md)
7. [Концепция функции и чистоты функции, сторонние эффекты](./7_Концепция_функции_и_чистоты_функции_сторонние_эффекты/README.md)
8. [Функции высокого порядка как концепция](./8_Функции_высшего_порядка_как_концепция/README.md)
9.  [Каррирование и частичное применение](./9_Каррирование_и_частичное_применение/README.md)
10. [Замыкания и декораторы](./10_Замыкания_и_декораторы/README.md)
11. [Функторы (объекты, синтаксически подобные функциям - поддерживающие операцию вызова - __call__)](./11_Функторы/README.md)
12. [Композиция функций](./12_Композиция_функций/README.md)
13. [Рекурсивные вычисления, фракталы, петли и пространства](./13_Рекурсивные_вычисления_фракталы_петли_и_пространства/README.md)
14. [TCO-оптимизация (tail call optimization)](./14_ТСО_оптимизация/README.md)
15. [Альтернативы циклам](./15_Альтернативы_циклам/README.md)
16. [Лямбда исчисление, Lambda в python](./16_Лямбда_исчисление/README.md)
17. [Теория категорий](./17_Теория_категорий/README.md)
18. [Типы данных, теория типов](./18_Типы_данных_и_теория_типов/README.md)
19. [Алгебраические типы данных](./19_Алгебраические_типы_данных/README.md)
20. [Классы типов](./20_Классы_типов/README.md)
21. [Иммутабельные типы данных](./21_Иммутабельные_типы_данных/README.md)
22. [Дескрипторы](./22_Десктипторы/README.md)
23. [Датаклассы и правила (+ заморозка)](./23_Датаклассы_и_правила_заморозка/README.md)
24. [Структурное сопоставление с шаблоном](./24_Структурное%20_сопоставление_с_шаблоном/README.md)
25. [Ленивость вычислений и ленивые вычисления в python](./25_Ленивость_вычислений/README.md)
26. [Протоколы итератора и генратора](./26_Протоколы_итератора_и_генератора/README.md)
27. [Включения](./27_Включения/README.md)
28. [Комбинаторика](./28_Комбинаторика/README.md)
29. [Монады](./29_Монады/README.md)
30. [Моноиды](./30_Моноиды/README.md)
31. [Заключение](./31_Заключение/README.md)
