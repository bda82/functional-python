"""
Одной из монад, реализуемых в python является монада List, которая работает со списками (последовательностями) и позволяет удобно выстраивать цепочки операций.
У монады List есть два основных метода: return (в других языках также называется unit) и bind (в Haskell известен как >>=).
    - return принимает один элемент и помещает его в список (контейнер монады).
    - bind берет список и функцию, которая возвращает список, применяет функцию к каждому элементу исходного списка и возвращает результат в виде нового списка.
Вот простая реализация монады List в Python:
"""
class ListMonad:
    def __init__(self, value=None):
        if value is None:
            self.value = []
        elif isinstance(value, ListMonad):
            self.value = value.value
        else:
            self.value = [value]

    def __rshift__(self, func):
        return ListMonad(
            [x for sublist in map(func, self.value) for x in sublist]
        )

    def __str__(self):
        return str(self.value)

    @staticmethod
    def unit(x):
        return ListMonad(x)

    @staticmethod
    def lift(func):
        def wrapper(*args, **kwargs):
            return ListMonad([func(*arg, **kw) for arg in args for kw in kwargs])
        return wrapper
    
"""
Давайте посмотрим на монаду List в действии на нескольких примерах. Предположим, у нас есть список чисел, и мы хотим применить функцию, которая возводит каждое число в квадрат, затем отфильтровывает нечетные числа и, наконец, суммирует полученные числа.
"""

numbers = [1, 2, 3, 4, 5]

@ListMonad.lift
def square(x):
    return x * x

@ListMonad.lift
def is_odd(x):
    return x % 2 != 0

@ListMonad.lift
def sum_list(numbers):
    return sum(numbers)

result = ListMonad.unit(numbers) >> square >> is_odd >> sum_list
print(result)  # ListMonad(50)

"""
В этом примере мы использовали монаду List для создания цепочки из трех функций, выполняющих возведение в квадрат, фильтрацию нечетных чисел и суммирование. Это демонстрирует, как монада List обеспечивает более декларативный и читаемый способ написания кода, упрощая последовательность операций над списками.
"""