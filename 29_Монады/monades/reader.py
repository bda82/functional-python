"""
Монада Reader в функциональном программировании представляет собой вычисление, которое может читать значения из общего окружения. В Python монады не являются встроенной концепцией, но их можно реализовать с помощью классов.

Вот пример реализации монады Reader в Python:
"""
class Reader:
    def __init__(self, run):
        self.run = run

    def map(self, f):
        return Reader(lambda r: f(self.run(r)))

    def flat_map(self, f):
        return Reader(lambda r: f(self.run(r)).run(r))
    
"""
В этом примере run - это функция, которая принимает окружение и возвращает результат. Метод map применяет функцию к результату, а метод flat_map применяет функцию, которая возвращает новый экземпляр Reader, к результату.

Вот пример использования этого класса для чтения значения из окружения:
"""

def get_value(key):
    return Reader(lambda env: env.get(key))

reader = Reader(lambda env: env)
new_reader = reader.flat_map(get_value('key'))
result = new_reader.run({'key': 'value'})
print(result)  # prints: value


"""
В этом примере мы создаем функцию get_value, которая возвращает новый экземпляр Reader, читающий значение из окружения по заданному ключу. Затем мы применяем эту функцию к начальному Reader с помощью flat_map.
"""

# еще примеры

# Использование монады Reader для доступа к конфигурации:
class Config:
    """
    В этом примере мы используем монаду Reader для доступа к конфигурации приложения. У нас есть две функции, get_db_host и get_db_user, которые возвращают Reader, читающий соответствующие значения из конфигурации
    """
    def __init__(self, db_host, db_user):
        self.db_host = db_host
        self.db_user = db_user

def get_db_host():
    return Reader(lambda config: config.db_host)

def get_db_user():
    return Reader(lambda config: config.db_user)

reader = Reader(lambda config: config)
new_reader = reader.flat_map(get_db_host)
new_reader2 = reader.flat_map(get_db_user)

config = Config('localhost', 'root')

print(new_reader.run(config))  # prints: localhost
print(new_reader2.run(config))  # prints: root

# Использование монады Reader для доступа к глобальному состоянию:
# В этом примере мы используем монаду Reader для доступа к глобальному состоянию. У нас есть функция get_global_state, которая возвращает Reader, читающий значение из глобального состояния по заданному ключу.
def get_global_state(key):
    return Reader(lambda state: state.get(key))

reader = Reader(lambda state: state)
new_reader = reader.flat_map(get_global_state('key'))

global_state = {'key': 'value'}

print(new_reader.run(global_state))  # prints: value