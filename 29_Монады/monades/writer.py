"""
Монада Writer в функциональном программировании представляет собой вычисление, которое производит некоторый вывод вместе с результатом вычисления. В Python монады не являются встроенной концепцией, но их можно реализовать с помощью классов.

Вот пример реализации монады Writer в Python:
"""
class Writer:
    def __init__(self, run):
        self.run = run

    def map(self, f):
        def run():
            a, log = self.run()
            return f(a), log
        return Writer(run)

    def flat_map(self, f):
        def run():
            a, log1 = self.run()
            b, log2 = f(a).run()
            return b, log1 + log2
        return Writer(run)
    
"""
В этом примере run - это функция, которая возвращает пару, состоящую из результата и лога. Метод map применяет функцию к результату, а метод flat_map применяет функцию, которая возвращает новый экземпляр Writer, к результату и объединяет логи.

Вот пример использования этого класса для логирования вычислений:
"""

def add(n):
    def run():
        return n, f"Added {n}. "
    return Writer(run)

writer = Writer(lambda: (0, ""))
new_writer = writer.flat_map(add(1)).flat_map(add(2))
result, log = new_writer.run()
print(result)  # prints: 3
print(log)  # prints: Added 1. Added 2. 

"""
В этом примере мы создаем функцию add, которая возвращает новый экземпляр Writer, добавляющий число к результату и записывающий это действие в лог. Затем мы применяем эту функцию к начальному Writer с помощью flat_map.
"""

# еще примеры
# Использование монады Writer для логирования шагов вычисления:
def multiply(n):
    def run():
        return n, f"Multiplied by {n}. "
    return Writer(run)

writer = Writer(lambda: (1, ""))
new_writer = writer.flat_map(multiply(2)).flat_map(multiply(3))
result, log = new_writer.run()
print(result)  # prints: 6
print(log)  # prints: Multiplied by 2. Multiplied by 3. 

# В этом примере мы создаем функцию multiply, которая возвращает новый экземпляр Writer, умножающий результат на заданное число и записывающий это действие в лог. Затем мы применяем эту функцию к начальному Writer с помощью flat_map.


# Использование монады Writer для логирования ошибок:
def safe_divide(n):
    def run():
        if n == 0:
            return None, "Division by zero. "
        else:
            return 1/n, f"Divided by {n}. "
    return Writer(run)

writer = Writer(lambda: (1, ""))
new_writer = writer.flat_map(safe_divide(0)).flat_map(safe_divide(2))
result, log = new_writer.run()
print(result)  # prints: None
print(log)  # prints: Division by zero. 

# В этом примере мы создаем функцию safe_divide, которая возвращает новый экземпляр Writer, безопасно выполняющий деление и записывающий ошибки в лог. Затем мы применяем эту функцию к начальному Writer с помощью flat_map.