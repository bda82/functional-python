"""
Монада State в функциональном программировании - это тип данных, который инкапсулирует вычисления, которые манипулируют некоторым состоянием. В Python монады не являются встроенной концепцией, но их можно реализовать с помощью классов.
Вот пример реализации монады State в Python:
"""
class StateMonad:
    def __init__(self, func):
        self.func = func

    def __rshift__(self, func):
        def wrapper(state):
            new_state, value = self.func(state)
            return func(value)(new_state)
        return StateMonad(wrapper)

    def __call__(self, state):
        return self.func(state)

    @staticmethod
    def unit(value):
        return StateMonad(lambda state: (state, value))

    @staticmethod
    def get():
        return StateMonad(lambda state: (state, state))

    @staticmethod
    def put(new_state):
        return StateMonad(lambda state: (new_state, None))
    

# Второй пример
"""
В этом примере run - это функция, которая принимает начальное состояние и возвращает пару, состоящую из результата и нового состояния. Метод map применяет функцию к результату, а метод flat_map применяет функцию, которая возвращает новый экземпляр State, к результату.

Вот пример использования этого класса для инкрементации значения.
"""
class State:
    def __init__(self, run):
        self.run = run

    def map(self, f):
        def run(s):
            a, s1 = self.run(s)
            return f(a), s1
        return State(run)

    def flat_map(self, f):
        def run(s):
            a, s1 = self.run(s)
            return f(a).run(s1)
        return State(run)

"""
В этом примере мы создаем функцию increment, которая возвращает новый экземпляр State, увеличивающий состояние на заданное значение. Затем мы применяем эту функцию к начальному состоянию с помощью flat_map.
"""
def increment(n):
    def run(s):
        return None, s + n
    return State(run)

state = State(lambda s: (None, s))
new_state = state.flat_map(increment(1))
_, state_value = new_state.run(0)
print(state_value)  # prints: 1


# третий пример 
class StateMonade:
    def __init__(self, run_func):
        self.run = run_func

    def __rshift__(self, func):
        return State(lambda s: func(self.run(s))[1])

    @staticmethod
    def get():
        return State(lambda s: (s, s))

    @staticmethod
    def put(new_state):
        return State(lambda _: (None, new_state))

    @staticmethod
    def modify(func):
        return State(lambda s: (None, func(s)))

    def eval(self, initial_state):
        return self.run(initial_state)[0]

    def exec(self, initial_state):
        return self.run(initial_state)[1]
    
"""
Монада State имеет два основных метода: get и put.

get возвращает текущее значение состояния.
put устанавливает состояние в новое значение.
Вот пример использования монады State для управления счетчиком, который увеличивается при каждом вызове функции:
"""
counter = StateMonade.modify(lambda x: x + 1)

# Chain the counter state monad with another state monad
result = (counter >> StateMonade.get()).eval(0)
print(result)  # 1

result = (counter >> counter >> StateMonade.get()).eval(0)
print(result)  # 3

"""
В этом примере мы использовали монаду State для инкремента счетчика и передачи значения состояния в течение всего вычисления.

Рассмотрим более практичный пример: реализацию простого генератора случайных чисел с затравкой (seed).
"""

import random

class RNG:
    def __init__(self, seed):
        self.seed = seed

    def next(self):
        self.seed = random.seed(self.seed)
        return self.seed.random()

rng_state = RNG(123)

@State.lift
def random_double(rng):
    return rng.next()

# Chain the random_double state monad with another state monad
result = (random_double >> random_double).eval(rng_state)
print(result)  # A tuple with two random floating-point numbers

"""
В этом примере мы использовали монаду State для управления состоянием генератора случайных чисел. С помощью цепочки из нескольких монад состояния random_double мы можем генерировать последовательность случайных чисел с одним и тем же семенем. Монада State позволяет более функционально и декларативно управлять состоянием и передавать его по цепочке во время серии вычислений.
"""
